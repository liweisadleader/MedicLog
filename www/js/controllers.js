angular.module('ml.controllers', [])

  .controller('DashCtrl', function($scope, $interval, Plan, Status, underscore) {    
    updateStatus();

    function updateStatus() {
      var status = {
        date: new Date(),
        todoList: []
      };
      $scope.plan = angular.copy(Plan.get());      
      angular.forEach($scope.plan.pills, function(pill) {
        for(var i = 1; i < pill.frequency + 1; i++) {
          var todo = {
            name: pill.name,
            frequency: pill.frequency,            
            index: i,
            quantity: pill.quantity,
            type: pill.type,
            completed: false
          };
          status.todoList.push(todo);
        }
      });
      
      $scope.status = angular.copy(Status.get());
      if(status.date.getDate() == $scope.status.date.getDate()) {
        angular.forEach(status.todoList, function(todo) {
          angular.forEach($scope.status.todoList, function(statusTodo) {
            if(todo.name === statusTodo.name && todo.index == statusTodo.index) {
              todo.completed = statusTodo.completed;
            }
          });
        });
      }
      $scope.status = angular.copy(status);
    }

    // Interactions
    $scope.toggleComplete = function(todo) {
      Status.save($scope.status);
      updateStatus();      
    }

    // Events
    $interval(updateStatus, 1000 * 60 * 10);
    $scope.$on('plan-updated', function() {
      $scope.plan = angular.copy(Plan.get());
      updateStatus();
    });
  })

  .controller('PlansCtrl', function($scope, Bucket, Plan, underscore, Uuid) {    
    $scope.mode = 'view';
    $scope.bucket = angular.copy(Bucket.get());
    console.log($scope.bucket);
    $scope.plan = angular.copy(Plan.get());
    initFormOptions();

    function initFormOptions() {
      $scope.formOptions = {
        name: underscore.pluck($scope.bucket.pills, 'name'),
        frequency: [
          {
            text: '每天1次',
            value: 1
          },
          {
            text: '每天2次',
            value: 2
          },
          {
            text: '每天3次',
            value: 3
          }
        ],
        quantity: [
          {
            text: '',
            value: 1
          },
          {
            text: '',
            value: 2
          },
          {
            text: '',
            value: 3
          }
        ]
      };
    };    



    // Interactions
    $scope.pillSelected = function(pillName) {
      var pill = underscore.find($scope.bucket.pills, function(pill) {
        return pill.name === pillName;
      });
      $scope.newPlanItem.type = pill.type;

      $scope.formOptions.quantity = underscore.map($scope.formOptions.quantity, function(quantity) {
        if( $scope.newPlanItem.type === '片剂' ) {
          quantity.text = '每次' + quantity.value + '片';
        }
        else if( $scope.newPlanItem.type === '颗粒' || $scope.newPlanItem.type === '胶囊' ) {
          quantity.text = '每次' + quantity.value + '粒';
        }
        else if( $scope.newPlanItem.type === '药膏' ) {
          quantity.text = '每次' + quantity.value + '克';
        }
        else if( $scope.newPlanItem.type === '液体' ) {
          quantity.text = '每次' + quantity.value + '滴';
        }
        else if( $scope.newPlanItem.type === '喷雾' ) {
          quantity.text = '每次噴' + quantity.value + '下';
        }
        return quantity;
      });
    }
    $scope.edit = function() {
      $scope.originalPlan = angular.copy($scope.plan);
      $scope.mode = 'edit';
    };
    $scope.add = function() {
      $scope.newPlanItem = {
        frequency: 1
      };
      $scope.originalPlan = angular.copy($scope.plan);
      $scope.mode = 'add';
    };
    $scope.cancel = function() {
      $scope.plan = angular.copy($scope.originalPlan);
      $scope.originalPlan = undefined;
      $scope.newPlanItem = undefined;
      $scope.mode = 'view';
    };
    $scope.save = function() {
      if($scope.newPlanItem) {
        $scope.newPlanItem.id = Uuid.getUuid();
        $scope.plan.pills.push($scope.newPlanItem);
      }
      $scope.originalPlan = undefined;
      $scope.newPlanItem = undefined;
      Plan.save($scope.plan);
      $scope.mode = 'view';
    };
    $scope.removeFromPlan = function(pill) {
      $scope.plan.pills = underscore.reject($scope.plan.pills, function(item) {
        return item.id === pill.id;
      });
    };

    // Events
    $scope.$on('bucket-updated', function() {
      $scope.bucket = angular.copy(Bucket.get());
      initFormOptions();
    });
  })

  .controller('BucketsCtrl', function($scope, Bucket, underscore, Uuid) {
    //$scope.$on('$ionicView.enter', function(e) {
    //});
    $scope.mode = 'view';
    $scope.bucket = angular.copy(Bucket.get());
    $scope.formOptions = {
      type: [ '片剂', '颗粒', '胶囊', '药膏', '液体', '喷雾' ]
    };

    // Interactions
    $scope.edit = function() {
      $scope.originalBucket = angular.copy($scope.bucket);
      $scope.mode = 'edit';
    };
    $scope.add = function() {
      $scope.newPillData = {
        type: '片剂',
        stock: 1
      };
      $scope.originalBucket = angular.copy($scope.bucket);
      $scope.mode = 'add';
    };
    $scope.cancel = function() {
      $scope.bucket = angular.copy($scope.originalBucket);
      $scope.originalBucket = undefined;
      $scope.newPillData = undefined;
      $scope.mode = 'view';
    };
    $scope.save = function() {
      if($scope.newPillData) {
        $scope.newPillData.id = Uuid.getUuid();
        $scope.bucket.pills.push($scope.newPillData);
      }
      $scope.originalBucket = undefined;
      $scope.newPillData = undefined;
      Bucket.save($scope.bucket);
      $scope.mode = 'view';
    };
    $scope.removeFromBucket = function(pill) {
      $scope.bucket.pills = underscore.reject($scope.bucket.pills, function(item) {
        return item.id === pill.id;
      });
    };
  })

// .controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
//   $scope.chat = Chats.get($stateParams.chatId);
// })

  .controller('AccountCtrl', function($scope) {
    $scope.settings = {
      enableFriends: true
    };
  });
