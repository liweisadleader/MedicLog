#MedicLog
一款基于cordova的提醒用戶按时服用药物的手机应用. 便于管理药品存量, 服用计划和按时提醒.
An HTML and Angular based phonegap application that reminds the users to
follow their own medical routines on time. (see more details in English below)

###APP 下載###
**Android和Windows Phone用戶：**

进入网址：[https://build.phonegap.com/apps/1673535/builds](https://build.phonegap.com/apps/1673535/builds)点击下载。

**IOS用户：**

App没有在Apple Store正式发布， 但感兴趣的用户可以通过 Xcode7 将应用免费推送到IPhone/IPad等设备。

具体方法见：[借助 XCode 7 免费部署和测试苹果 APP](http://www.codecraft.cn/article/55f92065a09a38412348e4ad)

###需要安装###
1. nodeJs， 详见[NodeJs官网](https://nodejs.org/en/)
2. Cordova，详见[Apache Cordova官网](http://cordova.apache.org/)

###如何開始###
**1. 克隆项目: `git clone https://git.oschina.net/liweisadleader/MedicLog.git`**

**2. 进入项目目录: `cd MedicLog`**

**3. 添加支持的移动操作系统，如`ios`或`android`：**

```
cordova platform add ios
cordova platform add android
```

**4. 准备项目，编译并运行：**

```
cordova prepare
cordova serve
```

**5. 在浏览器中测试：`localhost:8000/`**

運行第4步后，控制台會顯示模擬器運行的網址和端口，複製粘貼到瀏覽器即可。


###Download the APP###
**Android and Windows Phone Users**

Download from [https://build.phonegap.com/apps/1673535/builds](https://build.phonegap.com/apps/1673535/builds)

**IOS Users**
The App hasn't been published via Apple Store yet, but you are welcome to side push the app to any IOS devices with XCode7. More details regarding to Side Push with XCode7: [借助 XCode 7 免费部署和测试苹果 APP](http://www.codecraft.cn/article/55f92065a09a38412348e4ad)

###Dependencies###
1. nodeJs， See [NodeJs Official Site](https://nodejs.org/en/)
2. Cordova，See [Apache Cordova Official Site](http://cordova.apache.org/)

###How to get started###
**1. Clone the repository: `git clone https://git.oschina.net/liweisadleader/MedicLog.git`**

**2. cd to the project's folder: `cd MedicLog`**

**3. add platforms，如`ios`或`android`：**

```
cordova platform add ios
cordova platform add android
```

**4. Prepare and Run：**

```
cordova prepare
cordova serve
```

**5. Test it in the Browser：`localhost:8000/`**